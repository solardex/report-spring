# Spring Boot MySQL Report

This project is forked from https://github.com/hendisantika/spring-boot-mysql-report

An example to use Jasper Report in Spring Boot

Run this project by this command :

`mvn clean spring-boot:run`

Go to :
* http://localhost:8080
* http://localhost:8080/report/pdf --> To get PDF Report
* http://localhost:8080/report/excel2 --> To Get XLS Report


#### Screenshot

PDF Report

![PDF Report](img/pdf.png "PDF Report")

XLS Report

![XLS Report](img/xls.png "XLS report")
